const express = require('express');
const https = require('https');
const cors = require('cors');
const app = express();
const PORT = 5002;

app.use(express.json());
app.use(cors());

const PRICE_ENDPOINT = 'https://api.porssisahko.net/v1/price.json';

app.get('/getPrice', async (req, res) => {
  try {
    const dateAndTimeNow = new Date();
    const date = dateAndTimeNow.toISOString().split('T')[0];
    const hour = dateAndTimeNow.getHours();
    // Make a request to the API using the https module
    const fetch = await import('node-fetch');
    const response = await fetch.default(`${PRICE_ENDPOINT}?date=${date}&hour=${hour}`);
    if (!response.ok) {
      throw new Error(`HTTP Error: ${response.status} - ${response.statusText}`);
    }
    const responseData = await response.json();
    const price = responseData.price;
    res.json({ price });
  } catch (error) {
    console.error('Error:', error);
    res.status(500).json({ error: 'Error fetching price from the API' });
  }
});
app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}`);
});
