const express = require('express');
const https = require('https');
const app = express();
const cors = require('cors');
const mylib = require('./mylib');
const { DateTime } = require('luxon');
const port = 5002; 

app.use(express.json());
app.use(cors('*'));


const PRICE_ENDPOINT = 'https://api.porssisahko.net/v1/price.json';



app.get('/getPrice', async (req, res) => {
  try {
    const dateAndTimeNow = DateTime.now().setZone('Europe/Helsinki');
    console.log(dateAndTimeNow.toISO());

    const date = dateAndTimeNow.toISODate();
    const hour = dateAndTimeNow.hour;

    // Make a request to the API using the https module
    const fetch = await import('node-fetch');
    const response = await fetch.default(`${PRICE_ENDPOINT}?date=${date}&hour=${hour}`);
    if (!response.ok) {
      throw new Error(`HTTP Error: ${response.status} - ${response.statusText}`);
    }
    const responseData = await response.json();
    const price = responseData.price;
    res.json({ price });
  } catch (error) {
    console.error('Error:', error);
    res.status(500).json({ error: 'Error fetching price from the API' });
  }
});


// Endpoint to receive selectedDateTime from the frontend
app.post('/get-price-look-up', async (req, res) => {
  try {
    const { startDateTime, endDateTime } = req.body;
    console.log(`Start Date/Time: ${startDateTime}, End Date/Time: ${endDateTime}`);

    if (!startDateTime || !endDateTime) {
      res.status(400).json({ error: 'Invalid request. Please provide both startDateTime and endDateTime.' });
      return;
    }

    // Set the desired time zone (Helsinki)
    const helsinkiTimeZone = 'Europe/Helsinki';

    const priceData = await mylib.fetchLatestPriceData();

    if (Array.isArray(priceData.prices)) {
      // Convert the input date and time to Helsinki time zone
      const startDate = DateTime.fromISO(startDateTime).setZone(helsinkiTimeZone);
      const endDate = DateTime.fromISO(endDateTime).setZone(helsinkiTimeZone);

      // Assuming mylib.getPriceForDateRange function returns the price for a range of date and time.
      const price = mylib.getPriceForDateRange(startDate.toJSDate(), endDate.toJSDate(), priceData.prices);

      res.json({ price });
    } else {
      res.status(500).json({ error: 'Price data is not in the expected format' });
    }
  } catch (e) {
    res.status(500).json({ error: `Hinnan haku epäonnistui, syy: ${e}` });
  }
});

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});