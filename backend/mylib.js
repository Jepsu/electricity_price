const https = require('https');
const { DateTime } = require('luxon'); // Import Luxon for date and time handling

const LATEST_PRICES_ENDPOINT = 'https://api.porssisahko.net/v1/latest-prices.json';

function fetchLatestPriceData() {
  return new Promise((resolve, reject) => {
    https.get(LATEST_PRICES_ENDPOINT, (response) => {
      let data = '';

      response.on('data', (chunk) => {
        data += chunk;
      });

      response.on('end', () => {
        if (response.statusCode === 200) {
          try {
            const jsonData = JSON.parse(data, (key, value) => {
              if (key === 'startDate' || key === 'endDate') {
                return DateTime.fromISO(value, { zone: 'Europe/Helsinki' }).toJSDate();
              }
              return value;
            });
            resolve(jsonData);
          } catch (error) {
            reject('Failed to parse response JSON');
          }
        } else {
          reject(`Request failed with status code ${response.statusCode}`);
        }
      });
    }).on('error', (error) => {
      reject(`Request failed with error: ${error.message}`);
    });
  });
}

function getPriceForDate(date, prices) {
  const matchingPriceEntry = prices.find((price) => {
    const startDate = DateTime.fromJSDate(new Date(price.startDate), { zone: 'Europe/Helsinki' });
    const endDate = DateTime.fromJSDate(new Date(price.endDate), { zone: 'Europe/Helsinki' });
    return startDate <= date && endDate > date;
  });

  if (!matchingPriceEntry) {
    throw 'Price for the requested date is missing';
  }

  return matchingPriceEntry.price;
}

function getPriceForDateRange(startDate, endDate, prices) {
  const matchingPriceEntries = prices.filter((price) => {
    const priceStartDate = DateTime.fromJSDate(new Date(price.startDate), { zone: 'Europe/Helsinki' });
    const priceEndDate = DateTime.fromJSDate(new Date(price.endDate), { zone: 'Europe/Helsinki' });
    return priceStartDate >= startDate && priceEndDate <= endDate;
  });

  if (matchingPriceEntries.length === 0) {
    throw 'No prices found for the specified date/time range';
  }

  // Return the price from the first matching entry
  return matchingPriceEntries[0].price;
}


module.exports = {
  fetchLatestPriceData,
  getPriceForDate,
  getPriceForDateRange,
};
